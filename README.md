# 张庆松
## 2021.09.14
1. 文章阅读
 - [es6](https://es6.ruanyifeng.com/)
 - [ hooks 系列四：useRefs ](https://segmentfault.com/a/1190000040402752?utm_source=sf-similar-article
2. LeetCode 刷题
    - [ 加一 ](https://leetcode-cn.com/problems/plus-one/solution/)
3. 源码阅读
- [vue 源码--vue-router 根据源码实现方式](https://www.jianshu.com/p/3baa76ebbce0)
4. 项目进度
## 2021.09.13
1. 文章阅读
 - [es6](https://es6.ruanyifeng.com/)
 - [ hooks 系列四：useRefs ](https://segmentfault.com/a/1190000040402752?utm_source=sf-similar-article
2. LeetCode 刷题
    - [只出现一次的数字](https://leetcode-cn.com/problems/single-number/)
3. 源码阅读
- [vue 源码--vue-router 根据源码实现方式](https://www.jianshu.com/p/3baa76ebbce0)
4. 项目进度
# 黄文丽
## 2021.9.14
1. 1. 文章阅读
- [setState到底是异步还是同步?](https://juejin.cn/post/6850418109636050958)
2. 源码阅读
- [vue 源码 vue-router 根据源码实现方式](https://www.jianshu.com/p/3baa76ebbce0)
3. LeeCode刷题
  - [实现 strStr()](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnr003/)
4. 项目进度
## 2021.09.13
1. 文章阅读
- [还不会Vue3？一篇笔记带你快速入门](https://juejin.cn/post/7006518993385160711)
1. 源码阅读
- [原生实现一个 react-redux 的代码示例](https://www.jb51.net/article/141664.htm)
3. LeeCode 刷题
- [字符串相乘](https://leetcode-cn.com/leetbook/read/tencent/xxbolr/)
- [两数之和 II - 输入有序数组](https://leetcode-cn.com/problems/two-sum-ii-input-array-is-sorted/)
4. 项目进度
# 任永亮
## 2021.9.14
1. 文章阅读
   - [高级装饰器](https://juejin.cn/book/6844733813021491207/section/6844733813130526727)
2. 源码阅读
  - [事件系统](https://juejin.cn/post/6844903538762448910)
3. LeeCode刷题
4. 项目进度
    - [x] 评论管理
    - [x] 项目管理
## 2021.9.13
1. 文章阅读
   - [装饰器](https://juejin.cn/book/6844733813021491207/section/6844733813126332424)
2. 源码阅读
  - [事件系统](https://juejin.cn/post/6844903538762448910)
3. LeeCode刷题
4. 项目进度
    - [x] 评论管理
    - [x] 项目管理
# 马云龙
## 2021.9.14
1. 文章阅读
- [还不会Vue3？一篇笔记带你快速入门](https://juejin.cn/post/7006518993385160711)
1. 源码阅读
- [原生实现一个 react-redux 的代码示例](https://www.jb51.net/article/141664.htm)
3. LeeCode 刷题
- [括号生成](https://leetcode-cn.com/leetbook/read/tencent/x5ku2e/)
4. 项目进度
## 2021.9.13
1. 文章阅读
- [还不会Vue3？一篇笔记带你快速入门](https://juejin.cn/post/7006518993385160711)
1. 源码阅读
- [原生实现一个 react-redux 的代码示例](https://www.jb51.net/article/141664.htm)
3. LeeCode 刷题
- [字符串相乘](https://leetcode-cn.com/leetbook/read/tencent/xxbolr/)
- [两数之和 II - 输入有序数组](https://leetcode-cn.com/problems/two-sum-ii-input-array-is-sorted/)
4. 项目进度
# 王宇
## 2021.09.14
1. 文章阅读
- [ Vuex源码分析 ](https://www.cnblogs.com/ckAng/p/11065765.html)
- [ Vue3教程 ](https://blog.csdn.net/duninet/article/details/106472139)
2. LeetCode 刷题
- [ 加一 ](https://leetcode-cn.com/problems/plus-one/solution/)
3. 源码阅读
4. 项目进度
## 2021.09.13
1. 文章阅读
- [ JavaScript中的this指向问题 ](https://zhuanlan.zhihu.com/p/42145138)
- [ JavaScript 对象 ](https://www.jianshu.com/p/1a89512c970a)
2. LeetCode 刷题
- [ 加一 ](https://leetcode-cn.com/problems/plus-one/solution/)
3. 源码阅读
4. 项目进度
- [x] 渲染所有文章表格数据、完成排版
- [x] 渲染页面管理表格数据、完成排版
- [x] 完成分类管理、标签管理页面排版