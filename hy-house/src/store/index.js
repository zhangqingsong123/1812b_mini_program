import Vuex from 'vuex';
import Vue from 'vue';
import createLogger from 'vuex/dist/logger'
 
// 引入子模块
import user from './modules/user'
import add from './modules/add'
import home from './modules/home'
import chat from './modules/chat'
import house from './modules/house'

Vue.use(Vuex);


export default new Vuex.Store({
    modules: {
        user,
        add,
        home,
        chat,
        house
    },
    plugins: [createLogger()]
});